To run the storybook, use the command `npm run storybook` or `yarn storybook`.

The storybook should then be running at `localhost:9009`