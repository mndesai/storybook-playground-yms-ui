export const Colors = {
  leafyGreen: "#45b139",
  skyBlue: "#67b5ff",
  mango: "#ff9334",
  veryLightPink: "#f2f2f2",
  greyishBrown: "rgba(82, 82, 82, 0.7)",
  black: "#2d3848",
  darkCoral: "#d54e43"
};
