import React, { useCallback, useState } from "react";
import { DndProvider } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import { TrailerQueue } from "./TrailerQueueRow";
import TrailerTable from "./TrailerTable";
import CustomDragLayer from "./CustomDragLayer";


const SiteView = ({ trailers, queuedTrailers }) => {
  const [trailerQueue, setTrailerQueue] = useState(queuedTrailers);
  const handleDrop = item => {
    setTrailerQueue([...trailerQueue, item]);
  };
  return (
    <div>
      <DndProvider backend={HTML5Backend}>
        <TrailerQueue trailerQueue={trailerQueue} />
        <TrailerTable trailers={trailers} handleDrop={handleDrop} />
        <CustomDragLayer />
      </DndProvider>
    </div>
  );
};

export default SiteView;
