import React, { useState } from "react";
// import { ItemTypes } from "./SiteView";
import ItemTypes from "./ItemTypes";
import { DndProvider, useDrag, useDrop } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

export const TrailerQueue = ({ trailerQueue}) => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: ItemTypes.TRAILER_TABLE_ROW,
    drop: () => ({
      name: "Trailer Queue"
    }),
    collect: monitor => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    })
  });
  const borderColor = isOver ? "blue" : "black";
  return (
    <DndProvider backend={HTML5Backend}>
      <div
        ref={drop}
        style={{ border: `3px solid ${borderColor}`, width: 150 }}
      >
        <h3>Trailer Queue</h3>
        {trailerQueue.map(trailer => (
          <TrailerQueueRow trailer_name={trailer.trailer_name} />
        ))}
      </div>
    </DndProvider>
  );
};

const TrailerQueueRow = ({ trailer_name }) => {
  const [{ isDragging }, drag] = useDrag({
    item: { type: ItemTypes.TRAILER_QUEUE_ROW },
    collect: monitor => ({
      isDragging: !!monitor.isDragging()
    })
  });

  return (
    <div
      ref={drag}
      style={{
        border: "1px solid red",
        marginTop: 5,
        height: 30,
        display: "flex",
        alignItems: "center",
        paddingLeft: 15,
        backgroundColor: "red",
        opacity: isDragging ? 0.5 : 1,
        color: "white",
        fontWeight: 600,
        cursor: "move"
      }}
    >
      {trailer_name}
    </div>
  );
};

export default TrailerQueueRow;
