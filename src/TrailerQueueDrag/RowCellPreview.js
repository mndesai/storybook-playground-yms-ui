import React from "react";

const RowCellPreview = ({ data }) => {
  return (
    <div
      style={{
        border: "2px solid orange",
        width: 300,
        height: 100,
        display: "flex",
        alignItems: "center",
        paddingLeft: 20,
        fontWeight: 600,
        color: "white",
        backgroundColor: "orange"
      }}
    >
      {data.trailer_name}
    </div>
  );
};

export default RowCellPreview;
