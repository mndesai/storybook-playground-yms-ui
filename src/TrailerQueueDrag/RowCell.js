import React from "react";

const RowCell = ({ data }) => {
  return (
    <div
      style={{
        border: "1px solid blue",
        backgroundColor: "blue",
        fontWeight: 600,
        color: "white",
        cursor: "move"
      }}
    >
      {data}
    </div>
  );
};

export default RowCell;
