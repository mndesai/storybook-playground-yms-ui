import React from "react";
import ReactTable from "react-table";
import "./react-table.css";
import DraggableRowCell from "./DraggableRowCell";

const tableColumns = handleDrop => [
  {
    Header: "Trailer Name",
    id: "trailer_name",
    accessor: "trailer_name",
    Cell: row => {
      // return <RowCell data={row.value} handleDrop={handleDrop} />;
      return <DraggableRowCell data={row.value} handleDrop={handleDrop} />;
    }
  }
];

const TrailerTable = ({ trailers, handleDrop }) => {
  return (
    <ReactTable
      data={trailers}
      columns={tableColumns(handleDrop)}
      minRows={0}
    />
  );
};

export default TrailerTable;
