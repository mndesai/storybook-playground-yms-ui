import React from "react";
import { DragLayer, useDragLayer } from "react-dnd";
import ItemTypes from "./ItemTypes";
import RowCellPreview from "./RowCellPreview";
const layerStyles = {
  position: "fixed",
  pointerEvents: "none",
  zIndex: 100,
  left: 0,
  top: 0,
  width: "100%",
  height: "100%"
};

function getItemStyles(props) {
  const { currentOffset } = props;
  if (!currentOffset) {
    return {
      display: "none"
    };
  }

  const { x, y } = currentOffset;
  const transform = `translate(${x}px, ${y}px)`;
  return {
    transform: transform,
    WebkitTransform: transform
  };
}

function CustomDragLayer(props) {
  const { item, itemType, isDragging } = props;
  if (!isDragging) {
    return null;
  }

  function renderItem(type, item) {
    switch (type) {
      case ItemTypes.TRAILER_TABLE_ROW:
        return <RowCellPreview data={item}/>;
    }
  }

  return (
    <div style={layerStyles}>
      <div style={getItemStyles(props)}>{renderItem(itemType, item)}</div>
    </div>
  );
}

function collect(monitor) {
  return {
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging()
  };
}

export default DragLayer(collect)(CustomDragLayer);

// const CustomDragLayer = props => {
//   const { itemType, isDragging, item } = useDragLayer(monitor => ({
//     item: monitor.getItem(),
//     itemType: monitor.getItemType(),
//     isDragging: monitor.isDragging(),
//     initialOffset: monitor.getInitialSourceClientOffset(),
//     currentOffset: monitor.getSourceClientOffset()
//   }));
//   function renderItem() {
//     switch (itemType) {
//       case ItemTypes.TRAILER_TABLE_ROW:
//         return <RowCellPreview />;
//       default:
//         return null;
//     }
//   }
//   if (!isDragging) {
//     return null;
//   }
//   return (
//     <div>
//       <div>{renderItem()}</div>
//     </div>
//   );
// };
//
// export default CustomDragLayer;
