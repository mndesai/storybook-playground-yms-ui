import React from "react";
import RowCell from "./RowCell";
import { getEmptyImage } from "react-dnd-html5-backend";
import { DragSource } from "react-dnd";
import ItemTypes from "./ItemTypes";

class DraggableRowCell extends React.PureComponent {
  componentDidMount() {
    const { connectDragPreview } = this.props;
    if (connectDragPreview) {
      connectDragPreview(getEmptyImage(), {
        captureDraggingState: true
      });
    }
  }
  render() {
    const { connectDragSource, data } = this.props;
    return connectDragSource(
      <div>
        <RowCell data={data} />
      </div>
    );
  }
}

export default DragSource(
  ItemTypes.TRAILER_TABLE_ROW,
  {
    beginDrag(props) {
      const { data } = props;
      return { trailer_name: data}
    },
    endDrag(props) {
      const { handleDrop, data } = props;
      handleDrop({ trailer_name: data });
    }
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  })
)(DraggableRowCell);
