import React from "react";
import logo from "./logo.svg";
import "./App.css";
import SiteView from "./TrailerQueueDrag/SiteView";

const mockTrailersAvailable = [
  { trailer_name: "Trailer 1" },
  { trailer_name: "Trailer 2" },
  { trailer_name: "Trailer 3" },
  { trailer_name: "Trailer 4" }
];

const mockTrailersInQueue = [
  { trailer_name: "Trailer 5" },
  { trailer_name: "Trailer 6" },
  { trailer_name: "Trailer 7" }
];

function App() {
  return (
    <SiteView
      trailers={mockTrailersAvailable}
      queuedTrailers={mockTrailersInQueue}
    />
  );
}

export default App;
