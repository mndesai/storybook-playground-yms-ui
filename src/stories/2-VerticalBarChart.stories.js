import React from "react";
import VerticalBarChart from "../dashboard/styled-components/VerticalBarChart";
import { Colors } from "../styles/variables";

export default {
  title: "VerticalBarChart",
  component: VerticalBarChart
};

const textItems1 = [
  {
    number: 34,
    text: "Total Unique Active Switchers",
    numberColor: Colors.black
  },
  { number: 21, text: "Average per day", numberColor: Colors.black },
  { number: 19, text: "Active Today", numberColor: Colors.skyBlue }
];

const sampleData1 = [
  { x: 1, y: 11, date: "June 13th", fill: Colors.darkCoral },
  { x: 2, y: 17, date: "June 14th", fill: Colors.skyBlue },
  { x: 3, y: 14, date: "June 15th", fill: Colors.skyBlue },
  { x: 4, y: 17, date: "June 16th", fill: Colors.skyBlue },
  { x: 5, y: 14, date: "June 17th", fill: Colors.skyBlue },
  { x: 6, y: 17, date: "June 18th", fill: Colors.skyBlue }
];

export const VerticalBarChartExample = () => (
  <div
    style={{
      boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)",
      width: 700,
      height: 251
    }}
  >
    <VerticalBarChart
      highlightItems={textItems1}
      data={sampleData1}
      showToday
    />
  </div>
);

const textItems2 = [
  {
    number: 13,
    text: "Actual Check-ins Today",
    numberColor: Colors.leafyGreen
  },
  {
    number: 28,
    text: "Scheduled Check-ins For Today",
    numberColor: Colors.black
  }
];

const sampleData2 = [
  { x: 1, y: 9, date: "6/04/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 2, y: 14, date: "6/05/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 3, y: 17, date: "6/06/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 4, y: 14, date: "6/07/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 5, y: 12, date: "Yesterday", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 6, y: 16, date: "Today", fill: Colors.leafyGreen }
];

export const VerticalChartWithTranslucentBars = () => (
  <div
    style={{
      boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)",
      width: 700,
      height: 251
    }}
  >
    <VerticalBarChart highlightItems={textItems2} data={sampleData2} />
  </div>
);
