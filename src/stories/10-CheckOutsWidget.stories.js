import React from "react";
import CheckOutsWidget from "../dashboard/widgets/CheckOutsWidget";
import { Colors } from "../styles/variables";
import CheckInsWidget from "../dashboard/widgets/CheckInsWidget";

export default {
  title: "CheckOutsWidget",
  component: CheckOutsWidget
};

const textItems3 = [
  {
    number: 12.25,
    text: "Actual Check-outs Today",
    numberColor: Colors.skyBlue
  },
  {
    number: 28,
    text: "Scheduled Check-outs For Today",
    numberColor: Colors.black
  }
];

const sampleData3 = [
  { x: 1, y: 9, date: "6/04/20", fill: Colors.skyBlue, opacity: 0.65 },
  { x: 2, y: 14, date: "6/05/20", fill: Colors.skyBlue, opacity: 0.65 },
  { x: 3, y: 17, date: "6/06/20", fill: Colors.skyBlue, opacity: 0.65 },
  { x: 4, y: 14, date: "6/07/20", fill: Colors.skyBlue, opacity: 0.65 },
  { x: 5, y: 12, date: "Yesterday", fill: Colors.skyBlue, opacity: 0.65 },
  { x: 6, y: 16, date: "Today", fill: Colors.skyBlue }
];

export const BasicExample = () => {
  return (
    <div>
      <CheckOutsWidget data={sampleData3} highlights={textItems3} />
    </div>
  );
};
