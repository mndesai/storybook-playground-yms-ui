import React from "react";
import DashboardTabsContainer from "../dashboard/styled-components/DashboardTabsContainer";
import {
  CompletedLoadsExample,
  CompletedUnloadsExample,
  DoorTurnaroundExample
} from "./4-AverageCountWidget.stories";
import { BasicExample as TrailerThroughputExample } from "./5-TrailerThroughputWidget.stories";
import { BasicExample as ActiveSwitchersExample } from "./8-ActiveSwitchersWidget.stories";
import { BasicExample as MoveRequestTimesExample } from "./6-MoveRequestTimesWidget.stories";
import { BasicExample as CompletedMoveRequestsExample } from "./7-CompletedMoveRequests.stories";
import { BasicExample as CheckInOutButtons } from "./16-CheckInOutButtons.stories";
import { BasicExample as CheckInsExample } from "./9-CheckInsWidget.stories";
import { BasicExample as CheckOutsExample } from "./10-CheckOutsWidget.stories";

export default {
  title: "DashboardTabsContainer",
  component: DashboardTabsContainer
};

export const BasicExample = () => {
  const tab1Content = (
    <div>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <DoorTurnaroundExample />
        <TrailerThroughputExample />
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: 20
        }}
      >
        <CompletedUnloadsExample />
        <CompletedLoadsExample />
      </div>
    </div>
  );
  const tab2Content = (
    <div>
      <ActiveSwitchersExample />
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: 20
        }}
      >
        <MoveRequestTimesExample />
        <CompletedMoveRequestsExample />
      </div>
    </div>
  );
  const tab3Content = (
    <div>
      <CheckInOutButtons />
      <CheckInsExample />
      <CheckOutsExample />
    </div>
  );
  return (
    <DashboardTabsContainer
      tab1={tab1Content}
      tab2={tab2Content}
      tab3={tab3Content}
    />
  );
};
