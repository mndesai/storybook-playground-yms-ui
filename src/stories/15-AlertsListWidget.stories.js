import React from "react";
import AlertsList from "../dashboard/widgets/AlertsListWidget";

export default {
  title: "AlertsList",
  component: AlertsList
};

const alerts = [
  {
    id: 1,
    type: "warning",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Behind Schedule",
    read: false
  },
  {
    id: 2,
    type: "alert",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Excessive Dwell (1 Day 4 Hrs)",
    read: false
  },
  {
    id: 3,
    type: "inbound",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Delivery is 1 hour away",
    read: false
  },
  {
    id: 4,
    type: "alert",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Behind Schedule",
    read: false
  },
  {
    id: 5,
    type: "alert",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Behind Schedule",
    read: true
  },
  {
    id: 6,
    type: "warning",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Behind Schedule",
    read: true
  },
  {
    id: 7,
    type: "alert",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Excessive Well (1 Day 4 hrs)",
    read: true
  },
  {
    id: 8,
    type: "inbound",
    time: "12:30 AM",
    date: "Mon, Feb 4th 2019",
    label: "#10940194042 Delivery is 1 hour away",
    read: true
  }
];

export const BasicExample = () => {
  return (
    <div style={{ width: 380, height: 400 }}>
      <AlertsList alerts={alerts} />
    </div>
  );
};
