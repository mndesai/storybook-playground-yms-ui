import React, { useState } from "react";
import DashboardDropdown from "../dashboard/styled-components/DashboardDropdown";

export default {
  title: "DashboardDropdown",
  component: DashboardDropdown,
};

const locations = [
  { value: "1", label: "Dock A" },
  { value: "2", label: "Dock B" },
  { value: "3", label: "Dock C" },
  { value: "4", label: "Dock D" },
];

export const LocationsExample = () => {
  const [selected, setSelected] = useState([]);
  const onChangeHandler = (newSelected) => {
    setSelected(newSelected);
  };
  return (
    <DashboardDropdown
      label={"Locations"}
      options={locations}
      selected={selected}
      onChange={onChangeHandler}
    />
  );
};
