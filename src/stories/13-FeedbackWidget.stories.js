import React from "react";
import FeedbackWidget from "../dashboard/widgets/FeedbackWidget";

export default {
  title: "Feedback Widget",
  component: FeedbackWidget
};

export const BasicExample = () => {
  return (
    <div style={{ width: 380 }}>
      <FeedbackWidget onClick={() => console.log("provide feedback")} />
    </div>
  );
};
