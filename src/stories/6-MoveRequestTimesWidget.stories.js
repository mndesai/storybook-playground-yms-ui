import React from "react";
import MoveRequestTimesWidget from "../dashboard/widgets/MoveRequestTimesWidget";

export default {
  title: "MoveRequestTimesWidget",
  component: MoveRequestTimesWidget
};

export const BasicExample = () => {
  return (
    <div>
      <MoveRequestTimesWidget
        avgFull={"13m 21s"}
        avgAccepted={"53s"}
        avgStart={"4m 4s"}
        avgCompleted={"7m 17s"}
      />
    </div>
  );
};
