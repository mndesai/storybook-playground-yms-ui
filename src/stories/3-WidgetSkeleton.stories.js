import React from "react";
import WidgetSkeleton from "../dashboard/styled-components/WidgetSkeleton";

export default {
  title: "WidgetSkeleton",
  component: WidgetSkeleton
};

export const WidgetSkeletonSmall = () => (
  <div>
    <WidgetSkeleton title={"Small Widget"}>Small widget</WidgetSkeleton>
  </div>
);

export const WidgetSkeletonLarge = () => (
  <div>
    <WidgetSkeleton title={"Large Widget"} double>
      Large widget
    </WidgetSkeleton>
  </div>
);
