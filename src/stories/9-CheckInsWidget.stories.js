import React from "react";
import CheckInsWidget from "../dashboard/widgets/CheckInsWidget";
import { Colors } from "../styles/variables";

export default {
  title: "CheckInsWidget",
  component: CheckInsWidget,
};

const textItems2 = [
  {
    number: 13,
    text: "Actual Check-ins Today",
    numberColor: Colors.leafyGreen,
  },
  {
    number: 28,
    text: "Scheduled Check-ins For Today",
    numberColor: Colors.black,
  },
];

const sampleData2 = [
  { x: 1, y: 9, date: "6/04/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 2, y: 14, date: "6/05/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 3, y: 17, date: "6/06/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 4, y: 14, date: "6/07/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 5, y: 12, date: "Yesterday", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 6, y: 16, date: "Today", fill: Colors.leafyGreen },
];

export const BasicExample = () => {
  return (
    <CheckInsWidget
      data={sampleData2}
      highlights={textItems2}
      style={{ marginTop: 20, marginBottom: 20 }}
    />
  );
};

const sampleData3 = [
  { x: 1, y: 0, date: "6/04/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 2, y: 0, date: "6/05/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 3, y: 0, date: "6/06/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 4, y: 0, date: "6/07/20", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 5, y: 0, date: "Yesterday", fill: Colors.leafyGreen, opacity: 0.65 },
  { x: 6, y: 0, date: "Today", fill: Colors.leafyGreen },
];

export const AllValuesAreZero = () => {
  return (
    <CheckInsWidget
      data={sampleData3}
      highlights={textItems2}
      style={{ marginTop: 20, marginBottom: 20 }}
      maxY={10}
    />
  );
};
