import React from "react";
import UserGuidesWidget from "../dashboard/widgets/UserGuidesWidget";

export default {
  title: "UserGuidesWidget",
  component: UserGuidesWidget
};

export const BasicExample = () => {
  return (
    <div style={{ width: 380 }}>
      <UserGuidesWidget />
    </div>
  );
};
