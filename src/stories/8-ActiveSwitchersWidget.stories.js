import React from "react";
import ActiveSwitchersWidget from "../dashboard/widgets/ActiveSwitchersWidget";
import { Colors } from "../styles/variables";

export default {
  title: "ActiveSwitchersWidget",
  component: ActiveSwitchersWidget
};

const sampleData1 = [
  { x: 1, y: 11, date: "June 13th", fill: Colors.darkCoral },
  { x: 2, y: 17, date: "June 14th", fill: Colors.skyBlue },
  { x: 3, y: 14, date: "June 15th", fill: Colors.skyBlue },
  { x: 4, y: 17, date: "June 16th", fill: Colors.skyBlue },
  { x: 5, y: 14, date: "June 17th", fill: Colors.skyBlue },
  { x: 6, y: 17, date: "June 18th", fill: Colors.skyBlue }
];

const textItems1 = [
  {
    number: 34,
    text: "Total Unique Active Switchers",
    numberColor: Colors.black
  },
  { number: 21, text: "Average per day", numberColor: Colors.black },
  { number: 19, text: "Active Today", numberColor: Colors.skyBlue }
];

export const BasicExample = () => {
  return <ActiveSwitchersWidget data={sampleData1} highlights={textItems1} />;
};
