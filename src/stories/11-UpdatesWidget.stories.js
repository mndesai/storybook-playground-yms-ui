import React from "react";
import UpdatesWidget from "../dashboard/widgets/UpdatesWidget";

export default {
  title: "UpdatesWidget",
  component: UpdatesWidget
};

const messageText =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In scelerisque sed metus ac commodo. Phasellus rutrum pharetra mauris, quis convallis eros vehicula et. Morbi at nibh eu nunc condimentum pretium.";
const messageDate = "04/07/2020";

export const BasicExample = () => {
  return (
    <div style={{ border: "1px solid red", width: 800, padding: 30 }}>
      <UpdatesWidget message={messageText} date={messageDate} />
    </div>
  );
};
