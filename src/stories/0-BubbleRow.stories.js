import React from "react";
import BubbleRow from "../dashboard/styled-components/BubbleRow";
import { Colors } from "../styles/variables";

export default {
  title: "BubbleRow",
  component: BubbleRow
};

export const Total = () => (
  <div style={{ width: 100 }}>
    <BubbleRow
      bubbleColor={Colors.leafyGreen}
      text={"Total"}
      bubbleNumber={75}
    />
  </div>
);

export const TimeBubble = () => (
  <div style={{ width: 300 }}>
    <BubbleRow
      bubbleColor={Colors.skyBlue}
      bubbleNumber={"6:30"}
      text={"Dock A; Door 8"}
      bubbleStyle={{ width: 48 }}
    />
  </div>
);

export const MultipleRows = () => (
  <div>
    <BubbleRow
      bubbleColor={Colors.leafyGreen}
      text={"Total"}
      bubbleNumber={75}
    />
    <BubbleRow
      bubbleColor={Colors.skyBlue}
      text={"Dock A; Door 12"}
      bubbleNumber={33}
    />
    <BubbleRow
      bubbleColor={Colors.skyBlue}
      text={"Dock A; Door 14"}
      bubbleNumber={32}
    />
  </div>
);
