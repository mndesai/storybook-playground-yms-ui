import React from "react";
import AverageCountWidget from "../dashboard/styled-components/AverageCountWidget";
import { Colors } from "../styles/variables";
import DoorTurnaroundWidget from "../dashboard/widgets/DoorTurnaround";
import CompletedUnloadsWidget from "../dashboard/widgets/CompletedUnloads";
import CompletedLoadsWidget from "../dashboard/widgets/CompletedLoads";

export default {
  title: "AverageCountWidget",
  component: AverageCountWidget,
};

const sampleRows1 = [
  { fill: Colors.skyBlue, text: "Dock A; Door 12", number: "4:31" },
  { fill: Colors.skyBlue, text: "Dock A; Door 14", number: "4:55" },
  { fill: Colors.skyBlue, text: "Dock A; Door 1", number: "5:13" },
  { fill: Colors.skyBlue, text: "Dock A; Door 8", number: "6:12" },
  { fill: Colors.skyBlue, text: "Dock A; Door 8", number: "6:30" },
];

export const DoorTurnaroundExample = () => {
  return <DoorTurnaroundWidget averageCount={"12m 41s"} items={sampleRows1} />;
};

export const DoorTurnaroundNoData = () => {
  return <DoorTurnaroundWidget averageCount={"N/A"} items={[]} />;
};

const completedUnloads = [
  { fill: Colors.leafyGreen, text: "Total", number: 75 },
  { fill: Colors.skyBlue, text: "Dock A; Door 14", number: 32 },
  { fill: Colors.skyBlue, text: "Dock A; Door 1", number: 12 },
  { fill: Colors.skyBlue, text: "Dock A; Door 8", number: 8 },
  { fill: Colors.skyBlue, text: "Dock A; Door 7", number: 7 },
];

export const CompletedUnloadsExample = () => {
  return (
    <CompletedUnloadsWidget averageCount={"8:51"} items={completedUnloads} />
  );
};

export const CompletedLoadsExample = () => {
  return (
    <CompletedLoadsWidget averageCount={"8:51"} items={completedUnloads} />
  );
};
