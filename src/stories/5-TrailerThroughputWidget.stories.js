import React from "react";
import TrailerThroughputWidget from "../dashboard/widgets/TrailerThroughputWidget";

export default {
  title: "TrailerThroughputWidget",
  component: TrailerThroughputWidget
};

export const BasicExample = () => {
  return (
    <div>
      <TrailerThroughputWidget
        avgSite={"43m 53s"}
        avgDoor={"10m 4s"}
        avgYard={"43m 17s"}
      />
    </div>
  );
};
