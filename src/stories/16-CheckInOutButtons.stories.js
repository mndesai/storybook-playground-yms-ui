import React from "react";
import CheckInOutButtons from "../dashboard/widgets/CheckInOutButtons";

export default {
  title: "CheckInOutButtons",
  component: CheckInOutButtons
};

export const BasicExample = () => {
  return <CheckInOutButtons />;
};
