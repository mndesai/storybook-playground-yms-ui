import React from "react";
import CompletedMoveRequestsWidget from "../dashboard/widgets/CompletedMoveRequestsWidget";
import { Colors } from "../styles/variables";

export default {
  title: "CompletedMoveRequestsWidget",
  component: CompletedMoveRequestsWidget,
};

const items = [
  { number: 33, text: "Baldwin, Annie", fill: Colors.leafyGreen },
  { number: 33, text: "Manning, Jeffery", fill: Colors.skyBlue },
  { number: 12, text: "Hunt, Lelia", fill: Colors.skyBlue },
  { number: 8, text: "Wise, Nicholas", fill: Colors.skyBlue },
  { number: 7, text: "Wilkins, Alan", fill: Colors.skyBlue },
  { number: 5, text: "Alvarado, Harriet", fill: Colors.skyBlue },
  { number: 3, text: "Fowler, Alice", fill: Colors.skyBlue },
];

export const BasicExample = () => {
  return (
    <div>
      <CompletedMoveRequestsWidget items={items} />
    </div>
  );
};

export const NoMoveRequestsExample = () => {
  return (
    <div>
      <CompletedMoveRequestsWidget items={[]} />
    </div>
  );
};
