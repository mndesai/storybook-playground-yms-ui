import React from "react";
import TimeTextRow from "../dashboard/styled-components/TimeTextRow";
import { Colors } from "../styles/variables";

export default {
  title: "TimeTextRow",
  component: TimeTextRow
};

export const TimeTextRowWidgetHeader = () => (
  <div>
    <TimeTextRow
      timeText={"12m41s"}
      text={
        "Average time from Trailer Arrival at Door to Trailer Departing Door"
      }
      timeColor={Colors.leafyGreen}
      style={{ height: 90, borderBottom: "1px solid rgba(0,0,0.0.15)" }}
    />
  </div>
);

const sampleRows = [
  {
    timeText: "43m 53s",
    text: "Average Time on Site",
    timeColor: Colors.leafyGreen
  },
  { timeText: "10m 4s", text: "Average Time at Door", timeColor: Colors.mango },
  {
    timeText: "43m 17s",
    text: "Average Time in Yard",
    timeColor: Colors.leafyGreen
  }
];

export const TimeTextRowStacked = () => (
  <div
    style={{
      height: 305,
      width: 361,
      border: "1px solid red",
      display: "flex",
      flexDirection: "column"
    }}
  >
    {sampleRows.map(r => (
      <TimeTextRow
        timeText={r.timeText}
        text={r.text}
        timeColor={r.timeColor}
        style={{ flexGrow: 1, borderBottom: "1px solid rgba(0,0,0,0.15)" }}
      />
    ))}
  </div>
);
