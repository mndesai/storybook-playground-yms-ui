import React, { useCallback, useEffect, useState } from "react";
import _ from "lodash";
import ListItem from "./ListItem";
import PropTypes from "prop-types";

// Usage: SortableList can be used to create a drag-and-droppable list
// of custom components. The required props for SortableList are listId
// (to avoid drag/drop from one list to another), ListItemComponent (a custom
// component to be used as the list item), and items (an array of objects to be
// mapped over to create the list. An optional drop handler can be passed to SortableList
// (via prop onDrop) to be called when an item is successfully dragged
// and dropped within this list.
//
// The custom component (ListItemComponent) will be injected with additional
// drag-and-drop-related props (see ListItem.js for more info on these props
// as well as how to structure the custom list item component).
// Example implementation:
/*
const CustomSortableList = () => {
    const onDropHandler = (item, monitor, sortedItems) => {
        console.log("Dropped Item title:", item.itemInfo.title);
        console.log("Dropped Item color:", item.itemInfo.itemColor);
    }

    return <SortableList
      listId={'UNIQUE_LIST_ID'}
      ListItemComponent={<CustomItem propName1={propValue1} propName2={propValue2} />}
      items={[{title: "Item 1", id: 1, itemColor: "blue"}, {title: "Item 2", id: 2, itemColor: "red"}]}
      onDrop={onDropHandler}
    />
}
*/
// each item in the items array must have a unique id assigned to a field labeled "id"
//
// onDrop: the onDrop function will receive the following arguments: item, monitor, sortedItems
// * item: an object containing the item's info, by default these fields:
//      type: "list_item", used internally),
//      id: item's id (taken from the items array passed to SortableList)
//      index: item's index in the sorted list after it's been dropped successfully
//      listId: the list's listId
//      itemInfo: the original item from the items array
// * monitor: this is the DropTargetMonitor (see react-dnd for documentation on this)
// * sortedItems: the array of ordered items after a successful drop event
//

const SortableList = props => {
  const { items, listId, ListItemComponent, allowAdd } = props;
  const [sortedItems, setSortedItems] = useState([items]);
  useEffect(() => {
    setSortedItems(items);
  }, [items]);

  const moveHandler = useCallback(
    (item, hoverIdx) => {
      const dragIdx = sortedItems.findIndex(
        listItem => listItem.id === item.id
      );
      const newSortedItems = [...sortedItems];
      const dragItem = _.pullAt(newSortedItems, dragIdx)[0];
      const firstHalf = _.slice(newSortedItems, 0, hoverIdx);
      const secondHalf = _.slice(newSortedItems, hoverIdx);
      setSortedItems([...firstHalf, dragItem, ...secondHalf]);
    },
    [sortedItems]
  );

  const addHandler = useCallback((newItem, hoverIdx) => {
    const firstHalf = _.slice(sortedItems, 0, hoverIdx);
    const secondHalf = _.slice(sortedItems, hoverIdx);
    setSortedItems([...firstHalf, newItem.itemInfo, ...secondHalf]);
  });

  return (
    <div>
      {sortedItems.map((item, i) => (
        <ListItem
          ListItemComponent={ListItemComponent}
          listItem={item}
          index={i}
          key={`${listId}-${item.id}`}
          listId={listId}
          onDrop={props.onDrop}
          sortedItems={sortedItems}
          moveHandler={moveHandler}
          addHandler={addHandler}
          allowAdd={allowAdd}
        />
      ))}
    </div>
  );
};

SortableList.propTypes = {
  //  onDrop is the function that should fire when an item is dropped successfully at its target
  onDrop: PropTypes.func,
  //  items: an array of objects that will be mapped to items in the sortable list,
  //  each item must have a unique id, with a field in its object: {id: xxx}
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
    }).isRequired
  ).isRequired,
  //  listId: an id for the list to prevent errors when two lists are displayed at once
  //  and to prevent drag-and-drop addition from one list to another
  listId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  //  ListItemComponent: the custom component for displaying each item in the
  //  sortable list
  ListItemComponent: PropTypes.object.isRequired
};

export default SortableList;
