import React, { useRef } from "react";
import { useDrag, useDrop } from "react-dnd";

// Usage: ListItem is used in SortableList to inject drag-and-drop logic
// into a custom list item passed to SortableList (prop: ListItemComponent)
// There are a few requirements related to the structure of the ListItemComponent
// that need to be addressed for the drag-and-drop functionality to work as
// expected.
//
// When creating the custom list item component, the two main requirements are
// attaching previewRef and dragRef to the correct elements in your custom component.
// ListItem injects a few props into your custom component that can be accessed in that
//   component. These props are:
//    * itemInfo: this is each element in the items array passed to SortableList,
//    * previewRef: the ref that needs to be attached to the highest level
//      div that will appear to be dragged/dropped
//    * dragRef: the ref that needs to be attached to the drag handle within the
//      main dragging div that has the previewRef attached to it
//    * isDragging: a boolean telling your component if it's being dragged currently
//    * itemIndex: the item's current index in the list
//    * sortedItems: the current order of items in the list
//
// Example of a custom list item component:

/*
  CustomListComponent = (props) => {
    const {itemInfo, itemIndex, previewRef, dragRef} = props;
    return (
      <div>
        <div id="item_rank">{itemIndex}</div>
        <div id="draggable_content" ref={previewRef} style={{ border: "1px solid blue" }}>
          <div id="drag_handle" ref={dragRef} style={{cursor: "move"}}>Drag Handle</div>
          <div>
            <h1>My draggable item {itemInfo.name}</h1>
          </div>
        </div>
      <div>
    )
  }

  CustomSortableList = () => {

    const onDropHandler = (item, monitor, sortedItems) => {
      console.log("Dropped item:", item.itemInfo.name}
      console.log("New order:", sortedItems.map(item => item.name))
    }

    return <SortableList
      items={[{name: "Item 1", id: 1}, {name: "Item 2", id: 2}]}
      listId={101}
      ListItemComponent={CustomListComponent}
      onDrop={onDropHandler}
  }
*/

const ItemTypes = {
  LIST_ITEM: "list_item"
};

const ListItem = props => {
  const {
    listItem,
    moveHandler,
    index,
    listId,
    ListItemComponent,
    sortedItems,
    allowAdd
  } = props;
  const ref = useRef(null);
  const [, drop] = useDrop({
    accept: ItemTypes.LIST_ITEM,
    canDrop(item) {
      if (allowAdd) {
        return true;
      }
      return item.listId === listId;
    },
    hover(item, monitor) {
      if (!allowAdd && (!ref.current || item.listId !== listId)) {
        return;
      }
      if (allowAdd && item.listId !== listId) {
        // if item is not in list, add it to the list
        if (!sortedItems.map(s => s.id).includes(item.id)) {
          props.addHandler(item, index);
          console.log("adding");
        } else {
            console.log(monitor.isOver())
          // if item is already in list, do the regular hovering action
          const hoverIdx = index;
          const dragIdx = item.index;
          if (dragIdx === hoverIdx) {
            return;
          }
          const hoverBoundingRect = ref.current.getBoundingClientRect();
          const hoverMiddleY =
            (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
          const clientOffset = monitor.getClientOffset();
          const hoverClientY = clientOffset.y - hoverBoundingRect.top;
          if (dragIdx < hoverIdx && hoverClientY < hoverMiddleY) {
            return;
          }
          moveHandler(item, hoverIdx);
          item.index = hoverIdx;
        }
      } else {
        const hoverIdx = index;
        const dragIdx = item.index;
        if (dragIdx === hoverIdx) {
          return;
        }
        const hoverBoundingRect = ref.current.getBoundingClientRect();
        // Get vertical middle
        const hoverMiddleY =
          (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        // Determine mouse position
        const clientOffset = monitor.getClientOffset();

        // Get pixels to the top
        const hoverClientY = clientOffset.y - hoverBoundingRect.top;

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIdx < hoverIdx && hoverClientY < hoverMiddleY) {
          return;
        }
        //
        // // Dragging upwards
        if (dragIdx > hoverIdx && hoverClientY > hoverMiddleY) {
          return;
        }
        moveHandler(item, hoverIdx);
        item.index = hoverIdx;
      }
    },
    drop: (item, monitor) => {
      if (props.onDrop) {
        props.onDrop(item, monitor, sortedItems);
      }
    }
  });

  const [{ isDragging }, drag, preview] = useDrag({
    item: {
      type: ItemTypes.LIST_ITEM,
      id: listItem.id,
      index: index,
      listId: listId,
      itemInfo: listItem
    },
    collect: monitor => ({
      isDragging: monitor.isDragging()
    })
  });
  const opacity = isDragging ? 0 : 1;
  preview(drop(ref));

  // previewRef must be assigned as the ref for the div that represents the preview of the item being dragged
  // dragRef must be assigned as the ref for the drag handle of the draggable component

  return (
    <div style={{ opacity: opacity }}>
      {React.cloneElement(ListItemComponent, {
        itemInfo: listItem,
        previewRef: ref,
        dragRef: drag,
        isDragging,
        itemIndex: index,
        sortedItems
      })}
    </div>
  );
};

export default ListItem;
