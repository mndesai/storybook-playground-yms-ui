import React from "react";
import WidgetSkeleton from "../styled-components/WidgetSkeleton";
import VerticalBarChart from "../styled-components/VerticalBarChart";

const CheckOutsWidget = props => {
  const { data, highlights } = props;
  return (
    <WidgetSkeleton double title={"Check-Outs"}>
      <VerticalBarChart highlightItems={highlights} data={data} />
    </WidgetSkeleton>
  );
};

export default CheckOutsWidget;
