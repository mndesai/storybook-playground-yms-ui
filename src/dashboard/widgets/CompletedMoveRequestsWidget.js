import React from "react";
import WidgetSkeleton from "../styled-components/WidgetSkeleton";
import BubbleRow from "../styled-components/BubbleRow";

const CompletedMoveRequestsWidget = (props) => {
  const { items } = props;
  const widgetBody =
    items.length === 0 ? (
      <div
        style={{
          height: "100%",
          flexGrow: 1,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "rgba(115, 115, 115, 0.5)",
          fontStyle: "italic",
          fontWeight: 600,
          fontSize: 14,
        }}
      >
        No Data
      </div>
    ) : (
      items.map((item, i) => (
        <BubbleRow
          bubbleColor={item.fill}
          text={item.text}
          bubbleNumber={item.number}
          bubbleStyle={{ width: 48 }}
          // key={`count-bubble-${i}-${keyLabel}`}
        />
      ))
    );
  return (
    <WidgetSkeleton title={"Completed Move Requests By User"}>
      <div style={{ height: "100%", display: "flex", flexDirection: "column" }}>
        <div style={{ padding: 20, flexGrow: 1 }}>{widgetBody}</div>
      </div>
    </WidgetSkeleton>
  );
};

export default CompletedMoveRequestsWidget;
