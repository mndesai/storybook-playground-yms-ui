import React from "react";
import chatIcon from "../assets/stroke-1.svg";
import { Colors } from "../../styles/variables";

const FeedbackWidget = props => {
  const { onClick } = props;
  const titleComponent = (
    <div
      style={{
        fontSize: 16,
        display: "flex",
        alignItems: "center",
        marginBottom: 8
      }}
    >
      <img src={chatIcon} width={17} />
      <div style={{ marginRight: 8, marginLeft: 8 }}>Have Feedback?</div>
    </div>
  );
  const message = (
    <div
      style={{
        color: Colors.greyishBrown,
        fontSize: 13,
        fontWeight: 600,
        flexGrow: 1
      }}
    >
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. In scelerisque
      sed metus ac commodo.
    </div>
  );
  const feedbackButton = (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        backgroundColor: Colors.skyBlue,
        color: "white",
        fontSize: 14,
        fontWeight: 600,
        cursor: "pointer",
        borderRadius: 3
      }}
      onClick={onClick}
    >
      Provide Feedback
    </div>
  );
  return (
    <div
      style={{
        padding: "18px 20px 36px 20px",
        display: "flex",
        flexDirection: "column",
        height: 193,
        borderRadius: 3,
        boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)"
      }}
    >
      {titleComponent}
      {message}
      {feedbackButton}
    </div>
  );
};

export default FeedbackWidget;
