import React from "react";
import { Colors } from "../../styles/variables";

const CheckButton = props => {
  const { bgColor, text, style } = props;
  return (
    <div
      style={{
        height: 50,
        color: "white",
        fontSize: 14,
        fontWeight: 600,
        flexGrow: 1,
        backgroundColor: bgColor,
        borderRadius: 3,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
        ...style
      }}
    >
      {text}
    </div>
  );
};
const CheckInOutButtons = props => {
  return (
    <div
      style={{
        display: "flex",
        borderRadius: 3,
        border: "solid 1px #f4f5f6",
        padding: 20
      }}
    >
      <CheckButton bgColor={Colors.leafyGreen} text={"Go to Check-In Screen"} />
      <CheckButton
        bgColor={Colors.skyBlue}
        text={"Go to Check-Out Screen"}
        style={{ marginLeft: 20 }}
      />
    </div>
  );
};

export default CheckInOutButtons;
