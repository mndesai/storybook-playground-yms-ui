import React from "react";
import WidgetSkeleton from "../styled-components/WidgetSkeleton";
import TimeTextRow from "../styled-components/TimeTextRow";
import { Colors } from "../../styles/variables";

const commonStyles = {
  flexGrow: 1
};

const TrailerThroughputWidget = props => {
  const { avgSite, avgDoor, avgYard } = props;
  return (
    <WidgetSkeleton
      style={{ display: "flex", flexDirection: "column" }}
      title={"Trailer Throughput"}
    >
      <TimeTextRow
        timeText={avgSite}
        timeColor={Colors.leafyGreen}
        text={"Average Time on Site"}
        style={{
          ...commonStyles,
          borderBottom: "1px solid rgba(45, 56, 72, 0.05)"
        }}
      />
      <TimeTextRow
        timeText={avgDoor}
        timeColor={Colors.mango}
        text={"Average Time At Door"}
        style={{
          ...commonStyles,
          borderBottom: "1px solid rgba(45, 56, 72, 0.05)"
        }}
      />
      <TimeTextRow
        timeText={avgYard}
        timeColor={Colors.leafyGreen}
        text={"Average Time in Yard"}
        style={{ ...commonStyles }}
      />
    </WidgetSkeleton>
  );
};

export default TrailerThroughputWidget;
