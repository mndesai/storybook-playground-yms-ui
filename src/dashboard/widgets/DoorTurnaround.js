import React from "react";
import AverageCountWidget from "../styled-components/AverageCountWidget";
import { Colors } from "../../styles/variables";

const DoorTurnaroundWidget = props => {
  const { averageCount, items } = props;
  return (
    <div>
      <AverageCountWidget
        title={"Door Turnaround"}
        averageCount={averageCount}
        countColor={Colors.leafyGreen}
        headerText={
          "Average time from trailer arrival at door to trailer departing door"
        }
        items={items}
      />
    </div>
  );
};

export default DoorTurnaroundWidget;

