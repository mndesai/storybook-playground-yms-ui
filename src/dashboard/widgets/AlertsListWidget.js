import React from "react";
import { Colors } from "../../styles/variables";
import warning from "../assets/clock-line.svg";
import alert from "../assets/clock-line@3x.png";
import location from "../assets/location.svg";

const ReadIcon = () => (
  <div style={{ height: 16, display: "flex", alignItems: "center" }}>
    <div
      style={{
        width: 5,
        height: 5,
        backgroundColor: Colors.skyBlue,
        borderRadius: 5
      }}
    />
  </div>
);
const AlertItem = props => {
  const { type, time, date, label, read } = props;
  let icon = null;
  switch (type) {
    case "warning":
      icon = <img src={warning} alt={"warning"} width={16} />;
      break;
    case "alert":
      icon = <img src={alert} alt={"alert"} width={16} />;
      break;
    case "inbound":
      icon = <img src={location} alt={"inbound"} width={16} />;
      break;
    default:
      break;
  }

  return (
    <div
      style={{
        display: "flex",
        height: 60,
        paddingTop: 12,
        paddingLeft: 15,
        borderBottom: ".8px solid rgba(45, 56, 72, 0.2)",
        backgroundColor: !read ? "rgba(103, 182, 255, 0.07)" : "white"
      }}
    >
      <div style={{ width: 16, paddingTop: 2 }}>{!read && <ReadIcon />}</div>
      <div style={{ width: 20, paddingTop: 2 }}>{icon}</div>
      <div
        style={{
          flexGrow: 1,
          paddingRight: 15,
          paddingLeft: 5,
          paddingBottom: 12
        }}
      >
        <div style={{ fontSize: 13, fontWeight: 600, marginBottom: 3 }}>
          {label}
        </div>
        <div
          style={{ fontSize: 13, fontWeight: 600, color: Colors.greyishBrown }}
        >
          {time} | {date}{" "}
          <span
            style={{
              fontWeight: "normal",
              fontSize: 13,
              fontStyle: "italic",
              textDecoration: "underline",
              marginLeft: 8,
              cursor: "pointer"
            }}
          >
            Details
          </span>
        </div>
      </div>
    </div>
  );
};

const AlertsList = props => {
  const { alerts } = props;
  const numNewAlerts = alerts.filter(alert => !alert.read).length;
  const title = (
    <div
      style={{
        padding: "12px 20px",
        backgroundColor: Colors.black,
        color: "white",
        display: "flex",
        fontSize: 16,
        fontWeight: 600,
        alignItems: "center",
        borderRadius: "3px 3px 0 0",
        height: 30
      }}
    >
      Alerts
      <div
        style={{
          height: 25,
          backgroundColor: Colors.skyBlue,
          borderRadius: "50%",
          color: "white",
          fontSize: 14,
          fontWeight: 600,
          marginRight: 8,
          marginLeft: 11,
          padding: "0 10px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {numNewAlerts}
      </div>
      <div style={{ color: Colors.skyBlue, fontSize: 13, fontWeight: 600 }}>
        New
      </div>
    </div>
  );
  const alertsListItems = (
    <div style={{}}>
      {alerts.map(alert => (
        <AlertItem
          type={alert.type}
          time={alert.time}
          date={alert.date}
          label={alert.label}
          read={alert.read}
          key={`alert-${alert.id}`}
        />
      ))}
    </div>
  );
  return (
    <div
      style={{
        borderRadius: 3,
        boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)",
        height: "100%",
        display: "flex",
        flexDirection: "column"
      }}
    >
      {title}
      <div
        style={{
          overflowY: "auto",
          flexGrow: 1
        }}
      >
        {alertsListItems}
      </div>
    </div>
  );
};

export default AlertsList;
