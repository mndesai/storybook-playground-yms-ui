import React from "react";
import AverageCountWidget from "../styled-components/AverageCountWidget";
import { Colors } from "../../styles/variables";

const CompletedUnloadsWidget = props => {
  const { averageCount, items } = props;
  return (
    <div>
      <AverageCountWidget
        title={"Completed Loads"}
        averageCount={averageCount}
        countColor={Colors.leafyGreen}
        headerText={"Average time from trailer arrival to unload"}
        items={items}
      />
    </div>
  );
};

export default CompletedUnloadsWidget;
