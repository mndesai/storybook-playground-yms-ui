import React from "react";
import infoIcon from "../assets/info.svg";
import { Colors } from "../../styles/variables";

const DocLink = props => {
  const { label, onClick, style } = props;
  return (
    <div
      onClick={onClick}
      style={{
        color: Colors.greyishBrown,
        fontSize: 13,
        fontStyle: "italic",
        textDecoration: "underline",
        marginBottom: 15,
        cursor: "pointer",
        ...style
      }}
    >
      {label}
    </div>
  );
};

const UserGuidesWidget = props => {
  const titleComponent = (
    <div style={{ display: "flex", marginBottom: 19 }}>
      <img src={infoIcon} alt={"info"} width={20} />
      <div style={{ fontSize: 16, marginLeft: 8 }}>
        Need Help? Checkout the User Guides
      </div>
    </div>
  );

  const searchBar = (
    <div style={{ height: 45, border: "1px solid blue", marginBottom: 19 }}>
      Search
    </div>
  );
  const docLinks = (
    <div>
      <DocLink
        label={"Yard Insight Basics"}
        onClick={() => console.log("yard insight basics")}
      />
      <DocLink
        label={"Move Request Overview"}
        onClick={() => console.log("move request overview")}
      />
      <DocLink
        label={"See All Documentation"}
        onClick={() => console.log("see all documentation")}
        style={{ color: Colors.mango, marginBottom: 0 }}
      />
    </div>
  );
  return (
    <div
      style={{
        padding: "18px 20px 36px 20px",
        borderRadius: 3,
        boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)"
      }}
    >
      {titleComponent}
      {searchBar}
      {docLinks}
    </div>
  );
};

export default UserGuidesWidget;
