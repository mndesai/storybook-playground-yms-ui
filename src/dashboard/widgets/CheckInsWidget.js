import React from "react";
import WidgetSkeleton from "../styled-components/WidgetSkeleton";
import VerticalBarChart from "../styled-components/VerticalBarChart";

const CheckInsWidget = (props) => {
  const { data, highlights, style, maxY } = props;
  return (
    <div style={style}>
      <WidgetSkeleton double title={"Check-Ins"}>
        <VerticalBarChart highlightItems={highlights} data={data} maxY={maxY} />
      </WidgetSkeleton>
    </div>
  );
};

export default CheckInsWidget;
