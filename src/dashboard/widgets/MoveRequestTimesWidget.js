import React from "react";
import WidgetSkeleton from "../styled-components/WidgetSkeleton";
import TimeTextRow from "../styled-components/TimeTextRow";
import { Colors } from "../../styles/variables";

const commonStyles = {
  flexGrow: 1
};

const MoveRequestTimesWidget = props => {
  const { avgFull, avgAccepted, avgStart, avgCompleted } = props;
  return (
    <WidgetSkeleton
      style={{ display: "flex", flexDirection: "column" }}
      title={"Move Request Times"}
    >
      <TimeTextRow
        timeText={avgFull}
        timeColor={Colors.skyBlue}
        text={"Average Generation to Completed for 138 Moves"}
        style={{
          ...commonStyles,
          borderBottom: "1px solid rgba(45, 56, 72, 0.05)"
        }}
      />
      <TimeTextRow
        timeText={avgAccepted}
        timeColor={Colors.skyBlue}
        text={"Average Generation to Accepted"}
        style={{
          ...commonStyles,
          borderBottom: "1px solid rgba(45, 56, 72, 0.05)"
        }}
      />
      <TimeTextRow
        timeText={avgStart}
        timeColor={Colors.mango}
        text={"Average Accepted to Start"}
        style={{
          ...commonStyles,
          borderBottom: "1px solid rgba(45, 56, 72, 0.05)"
        }}
      />
      <TimeTextRow
        timeText={avgCompleted}
        timeColor={Colors.leafyGreen}
        text={"Average Start to Complete"}
        style={{
          ...commonStyles
        }}
      />
    </WidgetSkeleton>
  );
};

export default MoveRequestTimesWidget;
