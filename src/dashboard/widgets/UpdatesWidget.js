import React from "react";
import { Colors } from "../../styles/variables";
import alertIcon from "../assets/alert.svg";

const UpdatesWidget = props => {
  const { message, date } = props;
  const title = (
    <div style={{ display: "flex", alignItems: "center", marginBottom: 9 }}>
      <img src={alertIcon} alt={"alert"} width={20}/>
      <div
        style={{
          marginLeft: 7,
          fontSize: 16,
          fontWeight: 600
        }}
      >
        Platform Updates / Alerts
      </div>
    </div>
  );
  const messageComponent = (
    <div style={{ flexGrow: 1, fontSize: 13, fontWeight: 500 }}>
      {message} <span style={{ color: Colors.greyishBrown }}>{date}</span>
    </div>
  );
  const detailsLink = (
    <div
      style={{
        color: Colors.skyBlue,
        fontSize: 13,
        fontStyle: "italic",
        textDecoration: "underline",
        cursor: "pointer"
      }}
    >
      See More Details
    </div>
  );
  return (
    <div
      style={{
        borderRadius: 3,
        border: `1px solid ${Colors.skyBlue}`,
        backgroundColor: "rgba(103, 181, 255, 0.15)",
        minHeight: 138,
        padding: 20,
        display: "flex",
        flexDirection: "column"
      }}
    >
      {title}
      {messageComponent}
      {detailsLink}
    </div>
  );
};

export default UpdatesWidget;
