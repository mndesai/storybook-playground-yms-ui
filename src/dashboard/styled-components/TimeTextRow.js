import React from "react";

const TimeTextRow = props => {
  const { timeText, text, timeColor, style } = props;
  const timeComponent = (
    <div
      style={{
        color: timeColor,
        fontSize: 16,
        marginBottom: 4,
        fontWeight: 600
      }}
    >
      {timeText}
    </div>
  );
  const textComponent = (
    <div
      style={{
        color: "rgba(115, 115, 115, 0.7)",
        fontSize: 13,
        fontWeight: 600,
        textAlign: "center"
      }}
    >
      {text}
    </div>
  );
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        ...style
      }}
    >
      {timeComponent}
      {textComponent}
    </div>
  );
};

export default TimeTextRow;
