import React from "react";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
// import "react-tabs/style/react-tabs.css";
import "./custom-tabs.css";

const DashboardTabsContainer = props => {
  const { tab1, tab2, tab3 } = props;
  return (
    <div style={{ width: 780 }}>
      <Tabs>
        <TabList>
          <Tab>Door Performance</Tab>
          <Tab>Dispatch</Tab>
          <Tab>Inbound/Outbound</Tab>
        </TabList>

        <TabPanel>{tab1}</TabPanel>
        <TabPanel>{tab2}</TabPanel>
        <TabPanel>{tab3}</TabPanel>
      </Tabs>
    </div>
  );
};

export default DashboardTabsContainer;
