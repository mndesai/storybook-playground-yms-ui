import React from "react";

const BubbleRow = props => {
  const { bubbleColor, text, bubbleNumber, textStyle, bubbleStyle } = props;
  const bubble = (
    <div
      style={{
        backgroundColor: bubbleColor,
        color: "white",
        fontSize: 14,
        fontWeight: "bold",
        borderRadius: 25,
        width: 40,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: 25,
        ...bubbleStyle
      }}
    >
      {bubbleNumber}
    </div>
  );

  const textComponent = (
    <div
      style={{
        fontWeight: 600,
        fontSize: 13,
        marginLeft: 9,
        ...textStyle
      }}
    >
      {text}
    </div>
  );
  return (
    <div style={{ marginBottom: 10, display: "flex", alignItems: "center" }}>
      {bubble}
      {textComponent}
    </div>
  );
};

export default BubbleRow;
