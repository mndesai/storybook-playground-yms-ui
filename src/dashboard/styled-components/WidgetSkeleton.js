import React from "react";

const WidgetSkeleton = props => {
  const { double, title, style } = props;
  return (
    <div>
      <div
        style={{
          fontSize: 16,
          fontWeight: 600,
          marginBottom: 10
        }}
      >
        {title}
      </div>
      <div
        style={{
          borderRadius: 3,
          height: 305,
          width: double ? 740 : 360,
          backgroundColor: "#ffffff",
          boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.1)",
          ...style
        }}
      >
        {props.children}
      </div>
    </div>
  );
};

export default WidgetSkeleton;
