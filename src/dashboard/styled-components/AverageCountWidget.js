import React from "react";
import WidgetSkeleton from "./WidgetSkeleton";
import TimeTextRow from "./TimeTextRow";
import BubbleRow from "./BubbleRow";

const AverageCountWidget = (props) => {
  const { title, averageCount, countColor, headerText, items } = props;
  const widgetBody =
    items.length === 0 ? (
      <div
        style={{
          height: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          color: "rgba(115, 115, 115, 0.5)",
          fontStyle: "italic",
          fontWeight: 600,
          fontSize: 14,
        }}
      >
        No Data
      </div>
    ) : (
      items.map((item) => (
        <BubbleRow
          bubbleColor={item.fill}
          text={item.text}
          bubbleNumber={item.number}
          bubbleStyle={{ width: 48 }}
        />
      ))
    );
  return (
    <WidgetSkeleton title={title}>
      <div
        style={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <TimeTextRow
          timeText={averageCount}
          timeColor={countColor}
          text={headerText}
          style={{
            height: 90,
            borderBottom: "1px solid rgba(45, 56, 72, 0.05)",
          }}
        />
        <div style={{ padding: "20px", flexGrow: 1 }}>{widgetBody}</div>
      </div>
    </WidgetSkeleton>
  );
};

export default AverageCountWidget;
