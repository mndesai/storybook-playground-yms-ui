import React, { useState } from "react";
import Select from "react-select";
import { Colors } from "../../styles/variables";
import "react-select/dist/react-select.css";
import "./dashboard-dropdown.css";
import checkedIcon from "../assets/checked.svg";
import uncheckedIcon from "../assets/unchecked.svg";

const DropdownOption = (props) => {
  const { label, isSelected, onSelect, onDeselect, value } = props;
  const iconSrc = isSelected ? checkedIcon : uncheckedIcon;
  const onClick = isSelected ? () => onDeselect(value) : () => onSelect(value);
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        marginBottom: 12,
      }}
    >
      <div
        onClick={onClick}
        style={{ cursor: "pointer", display: "flex", alignItems: "center" }}
      >
        <img src={iconSrc} alt={"checked"} height={20} width={20} />
      </div>
      <div
        style={{
          fontSize: 13,
          fontWeight: 600,
          marginLeft: 9,
        }}
      >
        {label}
      </div>
    </div>
  );
};

const DashboardDropdownMenu = (props) => {
  const { options, onSelect, onDeselect, selected } = props;
  const [searchStr, setSearchStr] = useState("");
  return (
    <div style={{ padding: "11px 11px 0px 11px" }}>
      {options.map((opt) => (
        <DropdownOption
          key={opt.value}
          label={opt.label}
          value={opt.value}
          onSelect={onSelect}
          onDeselect={onDeselect}
          isSelected={selected.includes(opt.value)}
        />
      ))}
    </div>
  );
};

const DashboardDropdown = (props) => {
  const { label, options, selected, onChange } = props;
  const [currentSelected, setCurrentSelected] = useState([]);
  const onSelect = (val) => {
    setCurrentSelected([...currentSelected, val]);
  };
  const onDeselect = (val) => {
    setCurrentSelected(currentSelected.filter((s) => s !== val));
  };

  const submitHandler = () => {
    onChange(currentSelected);
    // setCurrentSelected([]);
  };
  // const openHandler = () => {
  //   setCurrentSelected(selected);
  // };

  const SelectedValuesComponent = () => {
    const selectedOptions = options.filter((opt) =>
      currentSelected.includes(opt.value)
    );

    let showLabel = "";
    if (selectedOptions.length === 0) {
      showLabel = "Select...";
    } else if (selectedOptions.length === options.length) {
      showLabel = "All";
    } else {
      showLabel = selectedOptions.map((opt) => opt.label).join(", ");
    }

    return (
      <div
        style={{
          height: 45,
          display: "flex",
          paddingLeft: 14,
          fontSize: 13,
          fontWeight: 600,
          alignItems: "center",
        }}
      >
        {showLabel}
      </div>
    );
  };
  return (
    <div>
      <div
        style={{
          color: Colors.greyishBrown,
          fontSize: 12,
          fontWeight: 600,
          marginBottom: 10,
        }}
      >
        {label}
      </div>
      <Select
        className={"dashboard"}
        searchable={false}
        placeholder={"All"}
        options={options}
        onClose={submitHandler}
        clearable={false}
        // onOpen={openHandler}
        valueComponent={SelectedValuesComponent}
        value={currentSelected}
        menuRenderer={(menuProps) => (
          <DashboardDropdownMenu
            {...menuProps}
            onChange={onChange}
            selected={currentSelected}
            onSelect={onSelect}
            onDeselect={onDeselect}
          />
        )}
      />
    </div>
  );
};

export default DashboardDropdown;
