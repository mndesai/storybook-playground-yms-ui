import React from "react";
import { Colors } from "../../styles/variables";
import { VictoryBar, VictoryChart, VictoryAxis, VictoryLabel } from "victory";

const HighlightItem = (props) => {
  const { number, text, numberColor } = props;
  const numberComponent = (
    <div
      style={{
        color: numberColor,
        fontSize: 16,
        fontWeight: "bold",
        marginBottom: 5,
      }}
    >
      {number}
    </div>
  );
  const textComponent = (
    <div
      style={{
        color: Colors.greyishBrown,
        textAlign: "center",
        fontSize: 13,
        fontWeight: 600,
      }}
    >
      {text}
    </div>
  );
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        flexGrow: 1,
        maxHeight: 100,
        padding: "0 30px",
      }}
    >
      {numberComponent}
      {textComponent}
    </div>
  );
};

const HighlightsBox = (props) => {
  const { highlightItems } = props;
  return (
    <div
      style={{
        backgroundColor: Colors.veryLightPink,
        width: 176,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        padding: "28px 0px",
      }}
    >
      {highlightItems.map((item) => (
        <HighlightItem
          number={item.number}
          text={item.text}
          numberColor={item.numberColor}
        />
      ))}
    </div>
  );
};

const chartGrey = "rgba(0,0,0,0.1)";

const Chart = (props) => {
  const { data, showToday, maxY } = props;
  const xLabels = data.map((d) => d.date);
  return (
    <div style={{ flexGrow: 1 }}>
      <VictoryChart
        height={251}
        domainPadding={{ x: 25 }}
        padding={{ left: 40, bottom: showToday ? 70 : 60, right: 20, top: 30 }}
      >
        <VictoryAxis
          dependentAxis
          style={{
            axis: { stroke: null },
            grid: { stroke: chartGrey }
          }}
          tickFormat={(t) => `${Math.round(t)}`}
          domain={maxY ? [0, maxY] : [0, 5]}
        />
        <VictoryBar
          data={data}
          barWidth={45}
          categories={{ x: xLabels }}
          style={{
            data: {
              fill: ({ datum }) => datum.fill,
              opacity: ({ datum }) => datum.opacity,
            },
          }}
        />
        <VictoryAxis
          style={{
            axis: { stroke: chartGrey },
          }}
        />
        {showToday && (
          <VictoryLabel
            text={"Today"}
            x={390}
            y={215}
            style={{
              fill: Colors.greyishBrown,
              fontSize: 12,
            }}
          />
        )}
      </VictoryChart>
    </div>
  );
};

const VerticalBarChart = (props) => {
  const { highlightItems, data, showToday, maxY } = props;
  return (
    <div
      style={{
        display: "flex",
        alignItems: "stretch",
        height: "100%",
      }}
    >
      <Chart data={data} showToday={showToday} maxY={maxY} />
      <HighlightsBox highlightItems={highlightItems} />
    </div>
  );
};

export default VerticalBarChart;
