var path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.js'],
  addons: [
    '@storybook/preset-create-react-app',
    '@storybook/addon-actions',
    '@storybook/addon-links',
  ],
    // webpackFinal: async (config, { configType }) => {
    //     // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    //     // You can change the configuration based on that.
    //     // 'PRODUCTION' is used when building the static version of storybook.
    //
    //     // Make whatever fine-grained changes you need
    //     config.module.rules.push({
    //         test: /\.js$/,
    //         // use: ['style-loader', 'css-loader', 'sass-loader'],
    //         use: 'babel-loader',
    //         // include: path.resolve(__dirname, '../'),
    //         exclude: /node_modules\/@webcomponents/
    //     });
    //
    //     // Return the altered config
    //     return config;
    // },
};

